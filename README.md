# OG Media

The main use case of this module is

* One content type as Organic Group
* One or more content types as Organic Group Content
* Media referenced from those nodes
* All referenced media types are Organic Group Content
* Every Organic Group Content has exactly one Group
* Media are attached to content via Entity Browser or Entity Embed

Parts of this may work in other setups, but this is not tested.

NOTE: 
* You need the patches lsited in composer.json. These are auto-applied
if you use the composer patches plugin and allow patches from modules.
* This was developed and tested using https://github.com/Gizra/og/pull/571

### Known issues
* Draft nodes are not shown in views lists any more
* Edit any group content permission is given to authors of group content
* OG: Media og_audience field is empty for non(group)-admin users, see https://github.com/Gizra/og/issues/638

## Helpers

This does nothing on its own but is required by other modules (EntityEmbed)

## Audience Match

* Validates that content and media group match on saving a node

## Audience Lock

* Prepends a "select group" step to the groupcontent create form
* Skips that step if user has no choice anyway (is only member of 1 group)
* Locks that choice in create step2 and edit form
* IF GroupStub module is enabled, saves groupcontent after step1 and redirects to
  the edit form.

## Group Stub

* Ensures that any edited group has an ID
  (and together with Audience Lock, any group content too)
* On requesting node/add/GROUP, saves a stub and redirects to the edit page
* Deletes unused stubs after 24h
* Prevents view access to the stub to hide it in content listings
* But allows the author to edit and view
* If session_node_access module is installed, this also works for anonymous

To configure,

* Enable core ContentModeration module
* Add a moderation workflow and attach your group(content) node types to it
* Add a moderation state with machine name ogmedia_group_stub
  which **must** have the "publish" property (to be selectable)
* Add a transition from that state to the *initial* state

## Upload Audience

To configure,

* In the entity browser media view, add a contextual filter by group id
* (If you use an Entity Browser Target Bundles context filter you need to remove it and add it as a regular filter)
* In the content type's media browser widget configure *Views Arguments* to pass
  the group ID (Which, for group, is *[node:nid]*, and for group content, 
  is *[node:og_audience:target_id]*)
  
Then,

* If the media browser also has an upload tab, then this views arguments will be 
  picked up for the media create form, the group will be auto-filled, and the
  group audience field will be disabled.

## Entity Embed

To configure,

* Add a media entity embed with a view, and an upload form

Then,

* If you embed from a group or group content page, view and upload page will find
  a views argument as if you configured them like described in *Upload Audience*. 
