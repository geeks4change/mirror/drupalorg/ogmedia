<?php

namespace Drupal\ogmedia_helpers;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Routing\RequestContext;
use Drupal\og\GroupTypeManagerInterface;
use Drupal\og\OgGroupAudienceHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class OGMediaHelpers
 *
 * Extracts and propagates groups info.
 */
class OGMediaHelpers implements OutboundPathProcessorInterface {

  const GROUPS_INFO = 'ogmedia_groups';

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\og\GroupTypeManagerInterface
   */
  protected $groupTypeManager;

  /**
   * @var \Drupal\og\OgGroupAudienceHelper
   */
  protected $groupAudienceHelper;

  /**
   * @var \Symfony\Component\Routing\RouterInterface
   */
  protected $router;

  /**
   * @var \Drupal\Core\PathProcessor\InboundPathProcessorInterface
   */
  protected $inboundPathProcessorManager;

  /**
   * OGMediaHelpers constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\og\GroupTypeManagerInterface $groupTypeManager
   * @param \Drupal\og\OgGroupAudienceHelper $groupAudienceHelper
   */
  public function __construct(RequestStack $requestStack, EntityTypeManagerInterface $entityTypeManager, GroupTypeManagerInterface $groupTypeManager, OgGroupAudienceHelper $groupAudienceHelper) {
    $this->requestStack = $requestStack;
    $this->entityTypeManager = $entityTypeManager;
    $this->groupTypeManager = $groupTypeManager;
    $this->groupAudienceHelper = $groupAudienceHelper;
  }

  /**
   * Get the no-access-check router.
   *
   * We need to get this at runtime to avoid circular dependencies.
   * (No, neither separating the pathProcessor nor setter injection resolve
   * this: "Circular reference detected for service "router.route_provider",
   * path: "router.route_provider -> path_processor_manager ->
   * ogmedia_helpers.path_processor -> ogmedia_helpers.service ->
   * router.no_access_checks"."
   *
   * @return \Symfony\Component\Routing\RouterInterface
   */
  public function router(): RouterInterface {
    if (!$this->router) {
      $this->router = \Drupal::service('router.no_access_checks');
    }
    return $this->router;
  }

  /**
   * @return \Drupal\Core\PathProcessor\InboundPathProcessorInterface
   */
  public function inboundPathProcessorManager(): InboundPathProcessorInterface {
    if (!$this->inboundPathProcessorManager) {
      $this->inboundPathProcessorManager = \Drupal::service('path_processor_manager');
    }
    return $this->inboundPathProcessorManager;
  }


  /**
   * Prepare groups info from origin header.
   *
   * This extracts and propagates groups from the origin entity, usually an edit
   * page. It is stored in a request attribute, and propagated in an equally
   * names querystring.
   * This is an ugly hack to compensate that entity_embed has no API to
   * propagate contextual info.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function prepareGroupsInfoFromReferrer(Request $request) {
    $info = NULL;
    if ($referrer = $request->headers->get('referer') ?? NULL) {
      $parameters = $this->matchRouteParameters($referrer);
      if (
        !empty($parameters['_entity_form'])
        && (list($entitiyTypeId) = explode('.', $parameters['_entity_form']))
        && ($entity = $parameters[$entitiyTypeId])
        && $entity instanceof EntityInterface
      ) {
        if ($this->groupTypeManager->isGroup($entitiyTypeId, $entity->bundle())) {
          $info = [$entitiyTypeId => [$entity->id()]];
        }
        elseif ($this->groupTypeManager->isGroupContent($entitiyTypeId, $entity->bundle())) {
          $fieldDefinitions = $this->groupAudienceHelper->getAllGroupAudienceFields($entitiyTypeId, $entity->bundle());
          $info = [];
          foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
            // This forgets the information, which field referenced the entity.
            // For now this is good enough.
            if (
              ($fieldItems = $entity->get($fieldName))
              && $fieldItems instanceof EntityReferenceFieldItemListInterface
              && ($groups = $fieldItems->referencedEntities())
            ) {
              foreach ($groups as $group) {
                $info[$group->getEntityTypeId()][] = $group->id();
              }
            }
          }
        }
      }
    }
    $this->setGroupsInfo($info);
  }

  public function hasGroupsInfo() {
    return $this->requestStack->getCurrentRequest()->attributes->has(static::GROUPS_INFO)
      || $this->requestStack->getCurrentRequest()->query->has(static::GROUPS_INFO);
  }

  public function getGroupsInfo() {
    return $this->requestStack->getCurrentRequest()->get(static::GROUPS_INFO);
  }

  public function setGroupsInfo(array $info = NULL) {
    return $this->requestStack->getCurrentRequest()->attributes->set(static::GROUPS_INFO, $info);
  }

  /**
   * {@inheritDoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // If someone else set the query string, leave as is. No need to add
    // cacheability either.
    if (!isset($options['query'][OGMediaHelpers::GROUPS_INFO])) {
      if ($bubbleable_metadata && $this->hasGroupsInfo()) {
        $bubbleable_metadata->addCacheContexts(['headers:referer']);
      }
      if ($groupsInfo = $this->getGroupsInfo()) {
        $options['query'][OGMediaHelpers::GROUPS_INFO] = $groupsInfo;
      }
    }
    return $path;
  }

  /**
   * @param $url
   *
   * @return array
   */
  public function matchRouteParameters($url): array {
    // @see \Drupal\Core\Path\PathValidator::getPathAttributes
    // @see \Drupal\system\PathBasedBreadcrumbBuilder::getRequestForPath
    $request = Request::create($url);
    $path = $this->inboundPathProcessorManager()
      // PathInfo already starts with a '/'.
      ->processInbound($request->getPathInfo(), $request);

    $savedContext = $this->router()->getContext() ?? new RequestContext();
    try {
      $context = new RequestContext();
      $context->fromRequest($request);
      $this->router()->setContext($context);
      $parameters = $this->router()->match($path);
    }
    catch (ResourceNotFoundException $e) {
      $parameters = [];
    }
    catch (ParamNotConvertedException $e) {
      $parameters = [];
    }
    catch (AccessDeniedHttpException $e) {
      $parameters = [];
    }
    catch (MethodNotAllowedException $e) {
      $parameters = [];
    }
    $this->router()->setContext($savedContext);
    return $parameters;
  }

}
