<?php

namespace Drupal\ogmedia_audience_match\Plugin\Validation\Constraint;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\media\MediaInterface;
use Drupal\og\Og;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OgMediaAudienceMatchConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritDoc}
   */
  public function validate($fieldItem, Constraint $constraint) {
    // Note that the name of a field item is its delta.
    if (
      $fieldItem instanceof FieldItemInterface
      && ($fieldItemList = $fieldItem->getParent())
      && $fieldItemList instanceof EntityReferenceFieldItemListInterface
      && ($referencedEntities = $fieldItemList->referencedEntities())
      && isset($referencedEntities[$fieldItem->getName()])
      && ($mediaEntity = $referencedEntities[$fieldItem->getName()])
      && $mediaEntity instanceof MediaInterface
    ) {
      $entity = $fieldItem->getEntity();
      /** @var \Drupal\og\OgGroupAudienceHelperInterface $ogGroupAudienceHelper */
      $ogGroupAudienceHelper = \Drupal::service(('og.group_audience_helper'));
      $mediaAudienceFields = $ogGroupAudienceHelper->getAllGroupAudienceFields($mediaEntity->getEntityTypeId(), $mediaEntity->bundle());

      // Is current entity a group content of same type as media? Validate it then.
      $entityAudienceFields = $ogGroupAudienceHelper->getAllGroupAudienceFields($entity->getEntityTypeId(), $entity->bundle());
      $audienceFieldNames = array_keys(array_intersect_key($entityAudienceFields, $mediaAudienceFields));
      foreach ($audienceFieldNames as $fieldName) {
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $entityAudienceField */
        $entityAudienceField = $entity->get($fieldName);
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $mediaAudienceField */
        $mediaAudienceField = $mediaEntity->get($fieldName);
        $this->validateMediaAudienceFieldHasAllGroups($fieldItem, $mediaAudienceField, $entityAudienceField->referencedEntities(), $constraint);
      }

      // Is current entity a group of media item? Validate it then.
      $groupBundleIds = Og::groupTypeManager()
        ->getGroupBundleIdsByGroupContentBundle('media', $mediaEntity->bundle());
      if (isset($groupBundleIds[$entity->getEntityTypeId()][$entity->bundle()])) {
        // @todo Fix this once needed.
        $fieldName = key($mediaAudienceFields);
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $mediaAudienceField */
        $mediaAudienceField = $mediaEntity->get($fieldName);
        $this->validateMediaAudienceFieldHasAllGroups($fieldItem, $mediaAudienceField, [$entity], $constraint);
      }
    }
  }

  /**
   * @param \Drupal\Core\Field\FieldItemInterface $fieldItem
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $mediaAudienceField
   * @param \Drupal\Core\Entity\EntityInterface[] $entityGroups
   * @param \Symfony\Component\Validator\Constraint $constraint
   */
  private function validateMediaAudienceFieldHasAllGroups(FieldItemInterface $fieldItem, EntityReferenceFieldItemListInterface $mediaAudienceField, array $entityGroups, Constraint $constraint): void {
    $mediaGroups = $mediaAudienceField->referencedEntities();
    $mediaGroupIds = array_map(function (EntityInterface $entity) {
      return $entity->id();
    }, $mediaGroups);
    /** @var \Drupal\Core\Field\FieldItemListInterface $fieldItemList */
    $fieldItemList = $fieldItem->getParent();

    foreach ($entityGroups as $entityGroup) {
      if (!in_array($entityGroup->id(), $mediaGroupIds)) {
        $params = [];
        $delta = intval($fieldItem->getName());
        $params['@delta'] = $delta + 1;
        $params['@media_field'] = $fieldItemList->getFieldDefinition()->getLabel();
        $params['@audience_field'] = $mediaAudienceField->getFieldDefinition()->getLabel();
        $params['%missing_audience'] = $entityGroup->label();
        $this->context->addViolation($constraint->notMatchingAudience, $params);
      }
    }
  }

}
