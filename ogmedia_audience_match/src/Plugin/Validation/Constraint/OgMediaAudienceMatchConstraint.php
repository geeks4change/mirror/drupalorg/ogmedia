<?php

namespace Drupal\ogmedia_audience_match\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * The constraint object.
 *
 * @Constraint(
 *   id = "ogmedia_audience_match",
 *   label = @Translation("OG Media Audience Match"),
 * )
 */
class OgMediaAudienceMatchConstraint extends Constraint {

  /**
   * Not a valid group message.
   *
   * @var string
   */
  public $notMatchingAudience = '@media_field: Media item @delta lacks parent %missing_audience @audience_field.';

}
